﻿using System;

namespace AdventOfCode.Level13
{
    public static partial class Level13
    {
        private sealed class L13Point : IEquatable<L13Point>
        {
            internal int X { get; set; }
            internal int Y { get; set; }

            internal L13Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString() => $"{X},{Y}";

            public override int GetHashCode() => X ^ Y;
            public override bool Equals(object obj)
            {
                if (obj is L13Point p)
                    return Equals(p);
                else
                    return false;
            }

            public bool Equals(L13Point other)
            {
                return other.X == this.X && other.Y == this.Y;
            }

            public static bool operator ==(L13Point a, L13Point b)
            {
                return Equals(a, b);
            }

            public static bool operator !=(L13Point a, L13Point b)
            {
                return !Equals(a, b);
            }
        }
    }
}