﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Level7
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level7.txt";

            Console.WriteLine("PART 1");
            DoIt(inputPath, totalWorkerCount: 1);

            WorkItem.IsPart2 = true;
            Console.WriteLine("\n\nPART 2");
            DoIt(inputPath, totalWorkerCount: 5);

            Console.ReadLine();
        }

        private static void DoIt(string inputPath, int totalWorkerCount)
        {
            List<WorkItem> workItems = FillWorkItems(inputPath);
            List<WorkItem> inProgressWorkItems = new List<WorkItem>();

            int secondsCounter = -1;
            while (workItems.Any(wi => !wi.IsDone) || inProgressWorkItems.Any(wi => !wi.IsDone))
            {
                // Make time pass
                for (int i = inProgressWorkItems.Count - 1; i >= 0; i--)
                {
                    WorkItem currentWorkItem = inProgressWorkItems[i];

                    currentWorkItem.RemainingWork--;

                    if (currentWorkItem.IsDone)
                    {
                        Console.Write(currentWorkItem.Name);
                        inProgressWorkItems.Remove(currentWorkItem);
                        workItems.ForEach(x => x.Requirements.Remove(currentWorkItem));
                    }
                }

                // Try to pick up items
                int itemsToStart = totalWorkerCount - inProgressWorkItems.Count;
                for (int i = 0; i < itemsToStart; i++)
                {
                    WorkItem firstAvailable = workItems.FirstOrDefault(x => x.Requirements.Count == 0);
                    if (firstAvailable == null)
                        break;

                    inProgressWorkItems.Add(firstAvailable);
                    workItems.Remove(firstAvailable);
                }

                secondsCounter++;
            }

            if (WorkItem.IsPart2)
                Console.WriteLine($"\nIt took {secondsCounter} seconds.");
        }

        private static List<WorkItem> FillWorkItems(string inputPath)
        {
            string[] fileRows = File.ReadAllLines(inputPath);

            Dictionary<char, WorkItem> itemReqDict = new Dictionary<char, WorkItem>();

            foreach (string fileRow in fileRows)
            {
                string[] rowParts = fileRow.Split(' ');
                char preReq = rowParts[1].Single();
                char current = rowParts[7].Single();

                if (!itemReqDict.ContainsKey(current))
                    itemReqDict[current] = new WorkItem(current);

                if (!itemReqDict.ContainsKey(preReq))
                    itemReqDict[preReq] = new WorkItem(preReq);

                itemReqDict[current].Requirements.Add(itemReqDict[preReq]);
            }

            return itemReqDict.Values.OrderBy(x => x.Name).ToList();
        }

        private class WorkItem
        {
            internal static bool IsPart2 = false;

            internal char Name { get; }
            internal int RemainingWork { get; set; }
            internal bool IsDone => RemainingWork == 0;

            internal readonly List<WorkItem> Requirements = new List<WorkItem>();

            internal WorkItem(char name)
            {
                Name = name;

                if (IsPart2)
                    RemainingWork = name - 'A' + 1 + 60;
                else
                    RemainingWork = 1;
            }

            public override string ToString() => $"{Name}, [ {string.Join(",", Requirements.Select(x => x.Name))} ]";
        }
    }
}