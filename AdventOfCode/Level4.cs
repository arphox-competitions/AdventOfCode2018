﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Level4
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level4.txt";

            string[] fileRows = File.ReadAllLines(inputPath);

            int? currentlyWorkingGuardId = null;
            DateTime currentlyWorkingSleepStart = DateTime.MinValue;

            Dictionary<int, long> guardIdToMinsSpentSleeping = new Dictionary<int, long>();
            Dictionary<int, Dictionary<DateTime, DateTime>> guardIdToAsleepTimes = new Dictionary<int, Dictionary<DateTime, DateTime>>();

            List<string> inputSorted = fileRows.OrderBy(GetTimeForOrdering).ToList();
            foreach (string row in inputSorted)
            {
                string[] rowSplit = row.Split(new char[] { ' ', '[', ']', '#' }, StringSplitOptions.RemoveEmptyEntries);
                DateTime dt = GetTime(rowSplit);

                if (rowSplit.Length == 6) // [1518-03-03 00:02] Guard #947 begins shift
                {
                    int guardId = int.Parse(rowSplit[3]);

                    if (!guardIdToMinsSpentSleeping.ContainsKey(guardId))
                        guardIdToMinsSpentSleeping.Add(guardId, 0);

                    currentlyWorkingGuardId = guardId;
                }
                else if (rowSplit[2] == "falls") // falls asleep
                {
                    currentlyWorkingSleepStart = dt;

                    if (!guardIdToAsleepTimes.ContainsKey(currentlyWorkingGuardId.Value))
                        guardIdToAsleepTimes[currentlyWorkingGuardId.Value] = new Dictionary<DateTime, DateTime>();

                    guardIdToAsleepTimes[currentlyWorkingGuardId.Value][dt] = DateTime.MinValue;
                }
                else if (rowSplit[2] == "wakes") // wakes up
                {
                    TimeSpan timeSpentSleeping = dt.Subtract(currentlyWorkingSleepStart);
                    guardIdToMinsSpentSleeping[currentlyWorkingGuardId.Value] += (long)timeSpentSleeping.TotalMinutes;

                    guardIdToAsleepTimes[currentlyWorkingGuardId.Value][currentlyWorkingSleepStart] = dt;
                }
                else
                {
                    throw new InvalidOperationException("problemo");
                }
            }

            Part1(guardIdToMinsSpentSleeping, guardIdToAsleepTimes);
            Part2(guardIdToAsleepTimes);
        }

        private static void Part1(Dictionary<int, long> guardIdToMinsSpentSleeping, Dictionary<int, Dictionary<DateTime, DateTime>> guardIdToAsleepTimes)
        {
            var ordered = guardIdToMinsSpentSleeping.OrderByDescending(x => x.Value).ToList();
            int worstGuardId = ordered.First().Key;
            var worstStat = guardIdToAsleepTimes[worstGuardId].OrderBy(x => x.Key).ToList();
            int maxIndex = GetMaxIndex(GetMinuteStatVector(worstStat));

            int part1answer = worstGuardId * maxIndex;
            Console.WriteLine(part1answer);
        }

        private static void Part2(Dictionary<int, Dictionary<DateTime, DateTime>> guardIdToAsleepTimes)
        {
            Dictionary<int, int> guardIdToMax = new Dictionary<int, int>();
            Dictionary<int, int> guardIdToMaxIndex = new Dictionary<int, int>();
            foreach (var item in guardIdToAsleepTimes)
            {
                int[] stat = GetMinuteStatVector(item.Value);
                int bestMinute = GetMaxIndex(stat);

                guardIdToMaxIndex[item.Key] = bestMinute;
                guardIdToMax[item.Key] = stat[bestMinute];
            }

            var guardIdToMaxORDERED = guardIdToMax.OrderByDescending(x => x.Value).ToList();
            int bestGuardId = guardIdToMaxORDERED.First().Key;
            int bestGuardMinute = guardIdToMaxIndex[bestGuardId];

            int part2answer = bestGuardId * bestGuardMinute;
            Console.WriteLine(part2answer);
        }

        private static int GetMaxIndex(int[] minToCount)
        {
            int maxIndex = 0;
            for (int i = 1; i < minToCount.Length; i++)
            {
                if (minToCount[i] > minToCount[maxIndex])
                {
                    maxIndex = i;
                }
            }

            return maxIndex;
        }

        private static int[] GetMinuteStatVector(IEnumerable<KeyValuePair<DateTime, DateTime>> worstStat)
        {
            int[] minToCount = new int[60];
            foreach (var stat in worstStat)
            {
                int x = stat.Key.Minute;
                int y = stat.Value.Minute;
                int low = Math.Min(x, y);
                int high = Math.Max(x, y);

                for (int i = low; i < high; i++)
                {
                    minToCount[i]++;
                }
            }

            return minToCount;
        }

        private static DateTime GetTimeForOrdering(string row)
        {
            string[] rowSplit = row.Split(new char[] { ' ', '[', ']', '#' }, StringSplitOptions.RemoveEmptyEntries);
            return GetTime(rowSplit);
        }

        private static DateTime GetTime(string[] rowSplit)
        {
            string[] dateParts = rowSplit[0].Split('-');
            int year = int.Parse(dateParts[0]);
            int month = int.Parse(dateParts[1]);
            int day = int.Parse(dateParts[2]);

            string[] timeParts = rowSplit[1].Split(':');
            int hour = int.Parse(timeParts[0]);
            int min = int.Parse(timeParts[1]);

            DateTime dt = new DateTime(year, month, day, hour, min, 0);
            return dt;
        }
    }
}