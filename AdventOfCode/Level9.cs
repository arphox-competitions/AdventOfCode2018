﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public sealed class Level9
    {
        private readonly int players;
        private readonly int lastMarbleWorth;
        private readonly long[] playerHighScores;
        private readonly LinkedList<int> circle = new LinkedList<int>();
        private LinkedListNode<int> currentItem;

        public Level9(int players, int lastMarbleWorth)
        {
            this.players = players;
            this.lastMarbleWorth = lastMarbleWorth;
            playerHighScores = new long[players];
            currentItem = circle.AddFirst(0);
        }

        public static void Run()
        {
            const string realPath = Program.PathBase + "level9.txt";
            (int players, int lastMarbleWorth) = GetInput(File.ReadAllText(realPath));

            // Part 1
            Level9 inst = new Level9(players, lastMarbleWorth);
            Console.WriteLine(inst.Calculate());

            // Part 2
            inst = new Level9(players, lastMarbleWorth * 100);
            Console.WriteLine(inst.Calculate());
        }

        public long Calculate()
        {
            for (int i = 1; i <= lastMarbleWorth; i++)
            {
                if (i % 23 == 0)
                {
                    GoBack(7);
                    playerHighScores[i % players] += i + currentItem.Value;
                    var tmp = currentItem;
                    GoForward();
                    circle.Remove(tmp);
                }
                else
                {
                    GoForward();
                    currentItem = circle.AddAfter(currentItem, i);
                }

                //Print(circle, currentItem.Value);
            }

            return playerHighScores.Max();
        }

        private void GoForward() => currentItem = currentItem.Next ?? circle.First;
        private void GoBack() => currentItem = currentItem.Previous ?? circle.Last;
        private void GoBack(int step) { for (int i = 0; i < step; i++) GoBack(); }

        private static void Print(LinkedList<int> circle, int currentValue)
        {
            Console.ResetColor();
            foreach (int item in circle)
            {
                Console.ForegroundColor = item == currentValue ? ConsoleColor.White : ConsoleColor.DarkGray;
                Console.Write(item + " ");
            }

            Console.ResetColor();
            Console.WriteLine();
        }

        public static (int players, int lastMarbleWorth) GetInput(string fileContent)
        {
            string[] split = fileContent.Split(' ');
            int players = int.Parse(split[0]);
            int lastMarbleWorth = int.Parse(split[6]);
            return (players, lastMarbleWorth);
        }
    }
}