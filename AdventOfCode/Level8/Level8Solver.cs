﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode.Level8
{
    public static partial class Level8Solver
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level8.txt";

            List<int> numbers = File.ReadAllText(inputPath).Split(' ').Select(int.Parse).ToList();

            TreeNode rootNode = Process(numbers);

            Console.WriteLine($"PART 1:\n{rootNode.Sum(x => x.Metadatas.Sum())}\n");
            Console.WriteLine($"PART 2:\n{rootNode.GetValue()}");
        }

        private static TreeNode Process(List<int> numbers)
        {
            TreeNode node = new TreeNode
            {
                ChildCount = numbers[0],
                MetadataCount = numbers[1]
            };

            numbers.RemoveRange(0, 2);

            if (node.ChildCount == 0)
            {
                node.Metadatas = numbers.GetRange(0, node.MetadataCount);
                numbers.RemoveRange(0, node.MetadataCount);
            }
            else
            {
                for (int i = 0; i < node.ChildCount; i++)
                    node.Children.Add(Process(numbers));

                node.Metadatas = numbers.GetRange(0, node.MetadataCount);
                numbers.RemoveRange(0, node.MetadataCount);
            }

            return node;
        }
    }
}