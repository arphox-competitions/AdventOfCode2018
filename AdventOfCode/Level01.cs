﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Level01
    {
        public static void Run()
        {
            string inputPath = InputProvider.GetInputFor(1);

            List<int> freqChanges = File.ReadAllLines(inputPath).Select(int.Parse).ToList();

            Console.WriteLine($"Part 1: {PartOne(freqChanges)}");
            Console.WriteLine($"Part 2: {PartTwo(freqChanges)}");
        }

        private static int PartOne(List<int> freqChanges)
        {
            return freqChanges.Sum();
        }

        private static int PartTwo(List<int> freqChanges)
        {
            HashSet<int> reachedFreqs = new HashSet<int>();
            reachedFreqs.Add(0);

            int currentFreq = 0;
            while (true)
            {
                foreach (int freqChange in freqChanges)
                {
                    currentFreq += freqChange;
                    if (reachedFreqs.Contains(currentFreq))
                    {
                        return currentFreq;
                    }

                    reachedFreqs.Add(currentFreq);
                }
            }
        }
    }
}