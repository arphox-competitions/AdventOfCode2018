﻿using Common;
using System;

namespace AdventOfCode
{
    public static class Program
    {
        public const string PathBase = @"..\..\..\data\";

        private static void Main()
        {
            string input = InputProvider.GetInputFor(15);
            new Level15.Solver().Solve();


            Console.ReadLine();
        }
    }
}