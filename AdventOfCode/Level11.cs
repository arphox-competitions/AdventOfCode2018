﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode
{
    public static class Level11
    {
        private const int serialNumber = 7989;

        public static void Run()
        {
            int[,] matrix = new int[301, 301]; // X, Y => row, col

            FillPowerValues(matrix);

            //SearchMax_Part1(matrix, out long maxSum, out int maxX, out int maxY);
            SearchMax_Part2(matrix);

            //PrintBestArea(matrix, maxSum, maxX, maxY, maxSize);
        }

        private static void PrintBestArea(int[,] matrix, long maxSum, int maxX, int maxY, int maxSize)
        {
            Console.WriteLine(maxSum);
            int toX = maxX + maxSize + 1;
            int toY = maxY + maxSize + 1;
            for (int x = maxX - 1; x < toX; x++)
            {
                for (int y = maxY - 1; y < toY; y++)
                {
                    Console.Write(matrix[x, y].ToString().PadLeft(2));
                }
                Console.WriteLine();
            }
        }

        private static void SearchMax_Part1(int[,] matrix, out long maxSum, out int maxX, out int maxY)
        {
            maxSum = 0; maxX = 0; maxY = 0;

            for (int x = 1; x < matrix.GetLength(0) - 3; x++)
            {
                for (int y = 1; y < matrix.GetLength(1) - 3; y++)
                {
                    // Go 3x3
                    long sum = 0;
                    for (int x2 = x; x2 < x + 3; x2++)
                        for (int y2 = y; y2 < y + 3; y2++)
                            sum += matrix[x2, y2];

                    if (sum > maxSum)
                    {
                        maxSum = sum;
                        maxX = x;
                        maxY = y;
                    }
                }
            }
        }

        private static void SearchMax_Part2(int[,] matrix)
        {
            long maxSum = 0;
            int maxX = 0, maxY = 0;
            int maxSize = 0;

            int matrixSizeX = matrix.GetLength(0);
            int matrixSizeY = matrix.GetLength(1);

            object maxLock = new object();

            Stopwatch stopper = Stopwatch.StartNew();
            Parallel.For(1, 301, size =>
            {
                int toX = matrixSizeX - size;
                int toY = matrixSizeY - size;

                for (int x = 1; x < toX; x++)
                {
                    for (int y = 1; y < toY; y++)
                    {
                        long sum = 0;
                        int toX2 = x + size;
                        int toY2 = y + size;
                        for (int x2 = x; x2 < toX2; x2++)
                            for (int y2 = y; y2 < toY2; y2++)
                                sum += matrix[x2, y2];

                        if (sum > maxSum)
                        {
                            lock (maxLock)
                            {
                                if (sum > maxSum)
                                {
                                    maxSum = sum;
                                    maxSize = size;
                                    maxX = x;
                                    maxY = y;
                                }
                            }
                        }
                    }
                }

                Console.WriteLine($"Size {size} done, current max size is {maxSize} at {maxX},{maxY}.");
            });

            Console.WriteLine($"{maxX},{maxY},{maxSize}");
            Console.WriteLine(stopper.Elapsed);
        }

        private static void FillPowerValues(int[,] matrix)
        {
            for (int x = 1; x < matrix.GetLength(0); x++)
            {
                for (int y = 1; y < matrix.GetLength(1); y++)
                {
                    int rackId = x + 10;
                    matrix[x, y] = rackId * y;
                    matrix[x, y] += serialNumber;
                    matrix[x, y] *= rackId;
                    matrix[x, y] = (matrix[x, y] / 100) % 10;
                    matrix[x, y] -= 5;
                }
            }
        }
    }
}
/*
Some benchmark results:
input: 7989
Release build, Define TRACE constant: OFF
Prefer 32-bit: OFF (if it is on, the code is slower by ~29% on my PC)

Times:
25.509, 24.790, 25.285, 25.237, 25.041, 24.755, 24.540, 25.002, 25.186, 25.355, 25.804, 24.766, 25.118, 24.996, 24.949, 26.106, 25.800
AVG: 25190 ms
*/