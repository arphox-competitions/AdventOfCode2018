﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override string ToString() => $"{X},{Y}";
    }

    public static class Level10
    {
        private const string realPath = Program.PathBase + "level10.txt";
        private const string demoPath = Program.PathBase + "level10demo.txt";
        private static readonly List<Point> points = new List<Point>();
        private static readonly List<Point> velocities = new List<Point>();

        public static void Run()
        {
            const string path = demoPath;
            Console.CursorVisible = false;

            string[] lines = File.ReadAllLines(path);

            foreach (string line in lines)
            {
                string[] parts = line.Split(new char[] { '=', '<', ' ', ',', '>' }, StringSplitOptions.RemoveEmptyEntries);
                int posX = int.Parse(parts[1]);
                int posY = int.Parse(parts[2]);
                points.Add(new Point(posX, posY));

                int velX = int.Parse(parts[4]);
                int velY = int.Parse(parts[5]);
                velocities.Add(new Point(velX, velY));
            }

            while (true)
            {
                Console.Clear();
                points.ForEach(p =>
                {
                    Console.SetCursorPosition((int)p.X + 6, (int)p.Y + 6);
                    Console.Write("#");
                });

                for (int i = 0; i < velocities.Count; i++)
                {
                    points[i].X += velocities[i].X;
                    points[i].Y += velocities[i].Y;
                }

                Console.ReadLine();
            }
        }
    }
}