﻿using System;

namespace AdventOfCode.Level12
{
    public static partial class Level12
    {
        public static void Run()
        {
            new Solver(Input.InitialState, Input.Rules).Run();
            Console.WriteLine("Finished.");
        }
    }
}