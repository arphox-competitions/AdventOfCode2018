﻿using System;
using System.Linq;
using System.Text;

/* Part 2 notes

    It seems that from the 102nd generation the figure gets stable, and takes this format (dots ('.') trimmed from start&end):

"###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###...###..###..###"

    And this whole thing gets shifted to the right with every iteration.
    1PV : first plant's value
    gen     1PV     SUM
    102     68      9306
    103     69      9375    
    104     70      9444
    105     71      9513
    106     72      9582

    So for gen x, the first plant's value is    >>> x - 34 <<<
    And the SUMs are increasing with 69 in every iteration.

    But we need only the gen and SUM relation:
    GEN   SUM
    102   9306
    103   9375
    104   9444
    105   9513
    106   9582
    107   9651
    108   9720
    109   9789
    110   9858
    111   9927
    112   9996

    So:            >>> 102 + x => 9306 + 69x <<
    Let's try for 111: (x = 9)
    102 + 9 => 9306 + 69*9
    111 => 9927         -> Good!

    So if the goal is 50 billion, x will be "49999999898" (50 billion - 102)
    So the sum is: 9306 + 69*49999999898 = 3450000002268.

    So this is the answer for part 2: 3450000002268

    Hell, Yeah! :D
    */

namespace AdventOfCode.Level12
{
    public static partial class Level12
    {
        private sealed class Solver
        {
            private static readonly bool PRINT_MODE_ON = true;

            private readonly Rule[] rules;

            private string currentState;
            private int currentZeroIndex = 0;

            internal Solver(string initialState, string[] rules)
            {
                if (initialState == null) throw new ArgumentNullException(nameof(initialState));
                if (rules == null) throw new ArgumentNullException(nameof(rules));

                this.rules = rules.Select(Rule.Parse).ToArray();
                currentZeroIndex = Extender.Extend(ref initialState, currentZeroIndex);
                currentState = initialState;
            }

            internal void Run()
            {
                // Uncomment only ONE part at a time, because they have shared data
                //Part1();
                Part2();
            }

            private void Part1()
            {
                int i = 0;
                while (i < 20)
                {
                    PrintCurrentState(i);
                    RunOneGeneration();
                    i++;
                }

                PrintCurrentState(i);

                int sum = GetSum(currentState, currentZeroIndex);
                Console.WriteLine($"\nSum: {sum}");
            }

            private void Part2()
            {
                int i = 0;
                while (true)
                {
                    PrintCurrentState(i);
                    RunOneGeneration();
                    i++;
                    Console.ReadLine();
                    Console.CursorTop--;
                }

                int sum = GetSum(currentState, currentZeroIndex);
                Console.WriteLine($"\nSum: {sum}");
            }

            private void RunOneGeneration()
            {
                currentZeroIndex = Extender.Extend(ref currentState, currentZeroIndex);
                StringBuilder newStateBuilder = new StringBuilder(new string('.', currentState.Length));

                for (int i = 0; i < currentState.Length - 5; i++)
                {
                    string subString = currentState.Substring(i, 5);
                    Rule ruleToApply = rules.SingleOrDefault(r => r.CanApply(subString));
                    if (ruleToApply != null)
                        newStateBuilder[i + 2] = ruleToApply.CenterNewState;
                }

                currentState = newStateBuilder.ToString();
            }

            private static int GetSum(string state, int currentZeroIndex)
            {
                int sum = 0;
                // Go backwards (including current)
                for (int i = currentZeroIndex; i >= 0; i--)
                {
                    if (state[i] == '#')
                    {
                        sum += i - currentZeroIndex;
                    }
                }

                // Go forward (excluding current)
                for (int i = currentZeroIndex + 1; i < state.Length; i++)
                {
                    if (state[i] == '#')
                    {
                        sum += i - currentZeroIndex;
                    }
                }

                return sum;
            }

            private void PrintCurrentState(int stateIndex)
            {
                if (!PRINT_MODE_ON)
                    return;

                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write($"{stateIndex:D3}: ");

                string trimmed = currentState.TrimStart('.');
                int firstPlantIndex = currentState.IndexOf('#');

                Console.WriteLine($"{trimmed}    1PV: {firstPlantIndex - currentZeroIndex}, SUM:{GetSum(currentState, currentZeroIndex)}");

                Console.ResetColor();


                //Console.ForegroundColor = ConsoleColor.Gray;
                //Console.Write($"{stateIndex:D3}: ");

                //string firstPart = currentState.Substring(0, currentZeroIndex);
                //string secondPart = currentState.Substring(currentZeroIndex + 1);

                //Console.ForegroundColor = ConsoleColor.Gray;
                //Console.Write(firstPart);

                //Console.ForegroundColor = ConsoleColor.Yellow;
                //Console.Write(currentState[currentZeroIndex]);

                //Console.ForegroundColor = ConsoleColor.Gray;
                //Console.WriteLine(secondPart);

                //Console.ResetColor();
            }
        }
    }
}