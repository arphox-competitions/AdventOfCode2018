﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode.Level6
{
    public static partial class Level6Solver
    {
        public static void Run()
        {
            const string inputPath = Program.PathBase + "level6.txt";
            const string demoInputPath = Program.PathBase + "level6demo.txt";

            List<Level6IPoint> integerPoints = File.ReadAllLines(inputPath).Select(Level6IPoint.ParseFromLine).ToList();
            DownScalePoints(integerPoints);

            int maxX = integerPoints.Max(x => x.X) + 1; // rows
            int maxY = integerPoints.Max(x => x.Y) + 1; // cols

            MapItem[,] map = new MapItem[maxX, maxY];
            InitMap(map, integerPoints);
            FillMap(integerPoints, map);
            FilterMap(map);

            ProcessMap_ForPart1(map);
            ProcessMap_ForPart2(map);
        }

        private static void DownScalePoints(List<Level6IPoint> integerPoints)
        {
            int minX = integerPoints.Min(x => x.X); // rows
            int minY = integerPoints.Min(x => x.Y); // cols

            foreach (var point in integerPoints)
            {
                point.X -= minX;
                point.Y -= minY;
            }
        }

        private static void InitMap(MapItem[,] map, List<Level6IPoint> integerPoints)
        {
            for (int col = 0; col < map.GetLength(0); col++)
            {
                for (int row = 0; row < map.GetLength(1); row++)
                {
                    map[col, row] = new MapItem(col, row);
                }
            }

            foreach (Level6IPoint p in integerPoints)
            {
                map[p.X, p.Y].RefPoint = p;
            }
        }

        private static void FillMap(List<Level6IPoint> integerPoints, MapItem[,] map)
        {
            foreach (var point in integerPoints)
            {
                for (int i = 0; i < map.GetLength(0); i++)
                {
                    for (int j = 0; j < map.GetLength(1); j++)
                    {
                        map[i, j].AddDistance(point, Math.Abs(point.X - i) + Math.Abs(point.Y - j));
                    }
                }
            }
        }

        private static void FilterMap(MapItem[,] map)
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    MapItem mapItem = map[i, j];

                    if (mapItem.IsRefPoint)
                        continue;

                    mapItem.PointDistances = mapItem.PointDistances.OrderBy(x => x.Value).ToList();
                }
            }
        }

        private static void ProcessMap_ForPart1(MapItem[,] map)
        {
            Dictionary<Level6IPoint, int> areas = new Dictionary<Level6IPoint, int>();

            foreach (var mapItem in map)
            {
                if (mapItem.IsRefPoint || mapItem.IsAmbigous())
                    continue;

                var best = mapItem.PointDistances.First();

                if (areas.ContainsKey(best.Key))
                    areas[best.Key]++;
                else
                    areas.Add(best.Key, 2); // start from 2 and not 1 because "including the coordinate's location itself"
            }

            List<KeyValuePair<Level6IPoint, int>> ordered = areas.OrderByDescending(x => x.Value).ToList();
            foreach (var item in ordered)
            {
                Console.WriteLine($"{item.Key} - {item.Value}");
            }

            Console.WriteLine($"PART 1:\n{ordered.First().Value}\n");
        }

        private static void ProcessMap_ForPart2(MapItem[,] map)
        {
            int size = map
                .Cast<MapItem>()
                .Count(x => x.SumOfDistances < 10000);

            Console.WriteLine($"PART 2:\n{size}\n");
        }
    }
}