﻿using System;

namespace AdventOfCode.Level6
{
    public static partial class Level6Solver
    {
        /// <summary>
        ///     Hidden class inside its superclass to not pollute intellisense in upcoming levels
        /// </summary>
        private sealed class Level6IPoint
        {
            internal int X { get; set; }
            internal int Y { get; set; }

            private Level6IPoint(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString() => $"{X};{Y}";

            internal static Level6IPoint ParseFromLine(string line)
            {
                string[] parts = line.Split(new string[] {", "}, StringSplitOptions.RemoveEmptyEntries);
                int x = int.Parse(parts[0]);
                int y = int.Parse(parts[1]);
                return new Level6IPoint(x, y);
            }
        }
    }
}