﻿using Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public static class Level14
    {
        private const int input = 509671;

        public static void Run()
        {
            // If you want to know the part 1, look for in the previous commit.
            Part2();
        }

        private static void Part2()
        {
            int elf1index = 0, elf2index = 1;
            List<int> numbers = new List<int>() { 3, 7 };

            const string myInput = "509671";
            List<int> desiredSequence = myInput.Select(x => (int)char.GetNumericValue(x)).ToList();
            int lastN = desiredSequence.Count;

            bool found = false;
            int offset = 0;

            while (!found)
            {
                int recipe1Value = numbers[elf1index];
                int recipe2Value = numbers[elf2index];
                int sum = recipe1Value + recipe2Value;
                if (sum < 10)
                {
                    numbers.Add(sum);
                }
                else
                {
                    numbers.Add(1);
                    numbers.Add(sum - 10);
                }

                elf1index = (elf1index + recipe1Value + 1) % numbers.Count;
                elf2index = (elf2index + recipe2Value + 1) % numbers.Count;

                if (numbers.TakeLast(lastN).SequenceEqual(desiredSequence))
                {
                    found = true;
                    offset = lastN;
                }
                else if (numbers.TakeLast(lastN + 1).Take(lastN).SequenceEqual(desiredSequence))
                {
                    found = true;
                    offset = lastN + 1;
                }
            }

            Console.WriteLine(numbers.Count - offset);
        }

        private static void Print(List<int> numbers, int elf1index, int elf2index)
        {
            for (int i = 0; i < numbers.Count; i++)
            {
                if (i == elf1index)
                    Console.ForegroundColor = ConsoleColor.Yellow;
                else if (i == elf2index)
                    Console.ForegroundColor = ConsoleColor.Green;
                else
                    Console.ForegroundColor = ConsoleColor.DarkGray;

                Console.Write(numbers[i] + " ");
            }

            Console.WriteLine();
        }
    }
}