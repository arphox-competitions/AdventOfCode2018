﻿using System.Collections.Generic;

namespace Level13
{
    internal sealed class CollisionInfo
    {
        internal List<Collision> Collisions { get; }
        internal bool DidCollisionHappen => Collisions.Count > 0;

        internal CollisionInfo(List<Collision> collisions)
        {
            Collisions = collisions;
        }
    }
}