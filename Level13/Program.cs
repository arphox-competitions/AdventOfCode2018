﻿using Common.Extensions;

namespace Level13
{
    class Program
    {
        static void Main()
        {
            bool isDemo = true;

            Run(isDemo);
        }

        private static void Run(bool isDemo)
        {
            string[] inputLines = Common.InputProvider.GetInputFor(13, isDemo)
                .SplitByCRLF();

            new Solver(inputLines).Run();
        }
    }
}