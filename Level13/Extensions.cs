﻿using System;
using System.Linq;

namespace Level13
{
    internal static class Extensions
    {
        internal static bool IsCartChar(this char ch) => "^>v<".Contains(ch);
        internal static T ItemAt<T>(this T[,] view, Point point) => view[point.X, point.Y];

        internal static void Move(this Point point, Facing facing)
        {
            switch (facing)
            {
                case Facing.Left: point.X--; break;
                case Facing.Right: point.X++; break;
                case Facing.Up: point.Y--; break;
                case Facing.Down: point.Y++; break;
                default: throw new ApplicationException();
            }
        }
    }
}