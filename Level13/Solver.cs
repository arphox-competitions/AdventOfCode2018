﻿using System;
using System.Linq;
using System.Threading;

namespace Level13
{
    class Solver
    {
        private Map map;

        public Solver(string[] inputLines)
        {
            map = new Map(inputLines);
        }

        public void Run()
        {
            while (map.Carts.Count > 1)
            {
                CollisionInfo collisionInfo = map.MoveCarts();
                //map.Print();

                if (collisionInfo.DidCollisionHappen)
                {
                    PrintCrashPosition(collisionInfo);
                    Console.ReadLine();
                }

                Thread.Sleep(80);
            }
        }

        private void PrintCrashPosition(CollisionInfo collisionInfo)
        {
            //Console.ForegroundColor = ConsoleColor.Red;
            //Console.WriteLine($"Collision at {map.CollisionPoint}, {map.Carts.Count} cart(s) left.");
        }

        private void PrintFinalPosition()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Last cart's position is {map.Carts.Single().Position}");
        }
    }
}