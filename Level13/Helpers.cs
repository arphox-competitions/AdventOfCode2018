﻿using System;
using System.Collections.Generic;

namespace Level13
{
    internal static class Helpers
    {
        private static readonly Dictionary<Facing, char> facingToChar = new Dictionary<Facing, char>()
        {
            { Facing.Left, '<' },
            { Facing.Right, '>' },
            { Facing.Up, '^' },
            { Facing.Down, 'v' }
        };

        private static readonly Dictionary<char, Facing> charToFacing = new Dictionary<char, Facing>()
        {
            { '<', Facing.Left },
            { '>', Facing.Right },
            { '^', Facing.Up },
            { 'v', Facing.Down }
        };

        internal static char FacingToChar(Facing facing) => facingToChar[facing];
        internal static Facing CharToFacing(char ch) => charToFacing[ch];

        internal static Facing GetNextFacing(Facing current, TurnStrategy turnStrategy)
        {
            switch (turnStrategy)
            {
                case TurnStrategy.Straight: return current;
                case TurnStrategy.Left: return TurnLeft(current);
                case TurnStrategy.Right: return TurnRight(current);
                default: throw new ApplicationException();
            }
        }

        internal static Facing TurnAtCornerIfNeeded(Facing facing, TurnStrategy turnStrategy, char mapTileUnderCart)
        {
            switch (mapTileUnderCart)
            {
                case '\\':
                    switch (facing)
                    {
                        case Facing.Right:
                        case Facing.Left: return TurnRight(facing);
                        case Facing.Down:
                        case Facing.Up: return TurnLeft(facing);
                    }
                    break;
                case '/':
                    switch (facing)
                    {
                        case Facing.Right:
                        case Facing.Left: return TurnLeft(facing);
                        case Facing.Down:
                        case Facing.Up: return TurnRight(facing);
                    }
                    break;
            }

            return facing; // No need to turn
        }

        internal static Facing TurnLeft(Facing facing)
        {
            switch (facing)
            {
                case Facing.Up: return Facing.Left;
                case Facing.Left: return Facing.Down;
                case Facing.Down: return Facing.Right;
                case Facing.Right: return Facing.Up;
                default: throw new ApplicationException();
            }
        }

        internal static Facing TurnRight(Facing facing)
        {
            switch (facing)
            {
                case Facing.Up: return Facing.Right;
                case Facing.Right: return Facing.Down;
                case Facing.Down: return Facing.Left;
                case Facing.Left: return Facing.Up;
                default: throw new ApplicationException();
            }
        }

        internal static TurnStrategy GetNextTurnStrategy(TurnStrategy turnStrategy)
        {
            switch (turnStrategy)
            {
                case TurnStrategy.Left: return TurnStrategy.Straight;
                case TurnStrategy.Straight: return TurnStrategy.Right;
                case TurnStrategy.Right: return TurnStrategy.Left;
                default: throw new ApplicationException();
            }
        }
    }
}