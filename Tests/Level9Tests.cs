﻿using AdventOfCode;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public sealed class Level9Tests
    {
        [TestCase(9, 25, 32, TestName = "Demo0")]
        [TestCase(10, 1618, 8317, TestName = "Demo1")]
        [TestCase(13, 7999, 146373, TestName = "Demo2")]
        [TestCase(17, 1104, 2764, TestName = "Demo3")]
        [TestCase(21, 6111, 54718, TestName = "Demo4")]
        [TestCase(30, 5807, 37305, TestName = "Demo5")]
        [TestCase(462, 71938, 398371, TestName = "Part1")]
        public void Level9TestCases(int playerCount, int lastMarbleWorth, long expectedValue)
        {
            Level9 solver = new Level9(playerCount, lastMarbleWorth);
            Assert.That(solver.Calculate(), Is.EqualTo(expectedValue));
        }
    }
}